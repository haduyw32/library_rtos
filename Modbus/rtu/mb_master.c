#include "system.h"
#include "mb_master.h"

extern void UART5_SEND(uint8_t* data, uint16_t size);
#define MODBUS_UARTx_SEND(x,y)                          UART5_SEND(x,y)

extern uint8_t buffer_uart5_rx[];
extern uint8_t buffer_uart5_rx_index;

HAL_StatusTypeDef mb_send_cmd(uint8_t* buffer_result, uint8_t slaveID, uint8_t function, uint16_t address, uint16_t number_of_registers, uint8_t* data, uint8_t len) {
    uint8_t buffer_len = 9 + len;
    uint8_t* buffer_cmd = (uint8_t*) sys_malloc (buffer_len);
    uint8_t pos = 0;
    buffer_cmd[pos++] = slaveID;
    buffer_cmd[pos++] = function;
    buffer_cmd[pos++] = address >> 8;
    buffer_cmd[pos++] = address & 0xFF;
    if (function != WRITE_SINGLE_COIL && function != WRITE_SINGLE_REGISTER) {
        buffer_cmd[pos++] = number_of_registers >> 8;
        buffer_cmd[pos++] = number_of_registers & 0xFF;
    }
    if (len != 0) {
        if (function != WRITE_SINGLE_COIL && function != WRITE_SINGLE_REGISTER) {
            buffer_cmd[pos++] = len;
        }
        for (uint8_t i = 0; i < len; i++) {
            buffer_cmd[pos++] = data[i];
        }
    }
    uint16_t crc = crc_check(buffer_cmd, pos);
    buffer_cmd[pos++] = crc & 0xFF;
    buffer_cmd[pos++] = crc >> 8;

    HAL_StatusTypeDef result;
    buffer_uart5_rx_index = 0;
    MODBUS_UARTx_SEND(buffer_cmd, pos);
    result = mb_receive(buffer_result, function);
    sys_free(buffer_cmd);
    return result;
}

HAL_StatusTypeDef mb_receive(uint8_t* buffer_result, uint8_t function) {
    HAL_StatusTypeDef result = HAL_TIMEOUT;
    for (uint16_t i=0; i<750; i++) {// 1s timeout
        uint8_t buffer_len;
        if (function != WRITE_SINGLE_COIL && function != WRITE_SINGLE_REGISTER) {
            buffer_len = buffer_uart5_rx[2] + 5;
        }
        else {
            buffer_len = 8;
        }
        if (buffer_uart5_rx_index != 0 && buffer_len == buffer_uart5_rx_index) {
            result = HAL_OK;
            break;
        }
        else {
            osDelay(2);
        }
    }

    if (result != HAL_OK) {
        // printf("ERR MODBUS: Timeout\r\n");
        buffer_uart5_rx_index = 0;
        return result;
    }

    memcpy(buffer_result, buffer_uart5_rx, buffer_uart5_rx_index);
    uint8_t len = buffer_result[2] + 5;
    uint16_t crc = crc_check(buffer_result, buffer_result[2] + 3);

    buffer_uart5_rx_index = 0;

    if ((crc >> 8) == buffer_result[len-1] && (crc & 0xFF) == buffer_result[len-2]) {
        return result;
    }
    else {
        printf("Err crc\r\n");
        return HAL_ERROR;
    }
}

uint16_t crc_check(uint8_t *ptr, int len)
{
	uint16_t crc16 = 0xFFFF;
	int i, j, tmp;
	for (i = 0; i < len; i++)
	{
		crc16 = *ptr ^ crc16;
		for (j = 0; j < 8; j++)
		{
			tmp = crc16 & 0x0001;
			crc16 = crc16 >> 1;
			if (tmp)
				crc16 = crc16 ^ 0xA001;
		}
		*ptr++;
	}
	return crc16;
}
//118.69.167.41