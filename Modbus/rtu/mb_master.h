#ifndef __MODBUS_MASTER_H
#define __MODBUS_MASTER_H

#ifdef __cplusplus
 extern "C" {
#endif

#define READ_COIL_STATUS                                1
#define READ_INPUT_STATUS                               2
#define READ_HOLDING_REGISTERS                          3
#define READ_INPUT_REGISTERS                            4
#define WRITE_SINGLE_COIL                               5
#define WRITE_SINGLE_REGISTER                           6
#define WRITE_MULTIPLE_COILS                            15

HAL_StatusTypeDef mb_send_cmd(uint8_t* buffer_content, uint8_t slaveID, uint8_t function, uint16_t address, uint16_t number_of_registers, uint8_t* data, uint8_t len);
HAL_StatusTypeDef mb_receive(uint8_t* buffer_content, uint8_t function);
uint16_t crc_check(uint8_t *ptr, int len);

#ifdef __cplusplus
}
#endif

#endif /* __MODBUS_MASTER_Hw */