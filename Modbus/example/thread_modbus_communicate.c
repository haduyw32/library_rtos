#include "../system/thread_list.h"
#include "mb.h"
#include "mbport.h"
#include "mb_master.h"

#define REG_START                                   1000
#define REG_STATUS_NREGS                            4

#define FREQUENCY_READ_DATA                         500
#define MODBUS_DEBUG                                1

extern UCHAR    ucMBAddress;
USHORT* usRegInputBuf = NULL;
USHORT usRegStatusBuf[REG_STATUS_NREGS];

void pdu_error_handle() {
    #if MODBUS_DEBUG == 1
    if (data_archives.debug[THREAD_MODBUS_COMMUNICATE_ID]) {
        UPRINT("pdu_error_handle\r\n");
    }
    #endif
    sys_timer_remove(THREAD_MODBUS_COMMUNICATE_ID, THREAD_MODBUS_ERROR);
    sys_timer_remove(THREAD_MODBUS_COMMUNICATE_ID, THREAD_MODBUS_READ_DATA);
    usRegStatusBuf[REG_STATUS_STATUS] = PDU_ERROR;
    usRegStatusBuf[REG_STATUS_CHILD] = 0;
    if (usRegStatusBuf[REG_STATUS_MODE] == PDU_MODE_SLAVE) {
        usRegStatusBuf[REG_STATUS_ID] = 1;
        ucMBAddress = 1;
    }

    sys_timer_add_nondata(THREAD_MODBUS_COMMUNICATE_ID, THREAD_MODBUS_FINDSLAVE, 1000, false);
    sys_timer_add_nondata(THREAD_MODBUS_COMMUNICATE_ID, THREAD_MODBUS_SETID, 6000, false);
}

void thread_modbus_communicate_func(void const * arg) {
    GPIO_Mode(MODBUS_MASTER_PORT_ENABLE, MODBUS_MASTER_PIN_ENABLE, GPIO_MODE_OUTPUT_PP);
    GPIO_Mode(MODBUS_SLAVE_PORT_ENABLE, MODBUS_SLAVE_PIN_ENABLE, GPIO_MODE_OUTPUT_PP);
    HAL_GPIO_WritePin(MODBUS_SLAVE_PORT_ENABLE, MODBUS_SLAVE_PIN_ENABLE, GPIO_PIN_RESET);
    data_archives.debug[THREAD_MODBUS_COMMUNICATE_ID] = 1;

    messenger_t  *msg;
    osEvent  evt;
    // sys_thread_suspend(THREAD_MODBUS_COMMUNICATE_ID);
    while (1) {
        evt = osMessageGet(thread_table[THREAD_MODBUS_COMMUNICATE_ID].MsgQId, osWaitForever);
        if (evt.status == osEventMessage) {
            msg = evt.value.p;
            switch (msg->signal) {
                case THREAD_MODBUS_FINDSLAVE: {
                    uint8_t data[] = {0, PDU_MODE_SLAVE};
                    uint8_t buffer_result[10];
                    if (mb_send_cmd(buffer_result, 1, WRITE_SINGLE_REGISTER, REG_STATUS_MODE + REG_START, 0, data, 2) != HAL_OK) {
                        #if (MODBUS_DEBUG == 1)
                        if (data_archives.debug[THREAD_MODBUS_COMMUNICATE_ID]) {
                            UPRINT("THREAD_MODBUS_FINDSLAVE: TIMEOUT\r\n");
                        }
                        #endif
                        sys_timer_add_nondata(THREAD_MODBUS_COMMUNICATE_ID, THREAD_MODBUS_FINDSLAVE, 2000, false);
                    }
                    else {
                        #if (MODBUS_DEBUG == 1)
                        if (data_archives.debug[THREAD_MODBUS_COMMUNICATE_ID]) {
                            UPRINT("Slave response\r\n");
                        }
                        #endif
                        usRegStatusBuf[REG_STATUS_CHILD] = 1;
                        usRegStatusBuf[REG_STATUS_STATUS] = PDU_ERROR;
                        sys_timer_add_nondata(THREAD_MODBUS_COMMUNICATE_ID, THREAD_MODBUS_SETID, 2000, false);
                    }
                    break;
                }
                case THREAD_MODBUS_SETID: {
                    sys_timer_remove(THREAD_MODBUS_COMMUNICATE_ID, THREAD_MODBUS_SETID);
                    if (usRegStatusBuf[REG_STATUS_MODE] == PDU_MODE_MASTER) {
                        #if (MODBUS_DEBUG == 1)
                        if (data_archives.debug[THREAD_MODBUS_COMMUNICATE_ID]) {
                            UPRINT("MODE: Master\r\n");
                        }
                        #endif
                        if (usRegStatusBuf[REG_STATUS_CHILD] != 0) {
                            uint8_t buffer_result[10];
                            uint8_t data[2];
                            data[0] = (usRegStatusBuf[REG_STATUS_ID] + 1) >> 8;
                            data[1] = (usRegStatusBuf[REG_STATUS_ID] + 1) & 0x00FF;
                            if (mb_send_cmd(buffer_result, 1, WRITE_SINGLE_REGISTER, REG_STATUS_ID + REG_START, 0, data, 2) != HAL_OK) {
                                // error handle
                                #if MODBUS_DEBUG == 1
                                if (data_archives.debug[THREAD_MODBUS_COMMUNICATE_ID]) {
                                    UPRINT("ERR: Master Set ID\r\n");
                                }
                                #endif
                                pdu_error_handle();
                                break;
                            }
                            else {
                                usRegStatusBuf[REG_STATUS_CHILD] = (((uint16_t)buffer_result[4] << 8) | buffer_result[5]) - usRegStatusBuf[REG_STATUS_ID];
                            }
                        }
                        #if MODBUS_DEBUG == 1
                        if (data_archives.debug[THREAD_MODBUS_COMMUNICATE_ID]) {
                            UPRINT("Number child: %d\r\n", usRegStatusBuf[REG_STATUS_CHILD]);
                        }
                        #endif
                        usRegStatusBuf[REG_STATUS_STATUS] = PDU_OK;
                        sys_free(usRegInputBuf); usRegInputBuf = NULL;
                        usRegInputBuf = (USHORT *)sys_malloc(REG_INPUT_NREGS * sizeof(USHORT) * (usRegStatusBuf[REG_STATUS_CHILD] + 1));
                        sys_timer_add_nondata(THREAD_MODBUS_COMMUNICATE_ID, THREAD_MODBUS_READ_DATA, usRegStatusBuf[REG_STATUS_CHILD] * FREQUENCY_READ_DATA, false);
                    }
                    break;
                }
                case THREAD_MODBUS_REQUEST_SLAVE: {
                    break;
                }
                case THREAD_MODBUS_READ_DATA: { //thread_push_messenger_nondata(THREAD_SERVER_INTERFACE_ID, THREAD_SERVER_TRAP_LINKUP);
                    // read data //
                    HAL_StatusTypeDef result = HAL_OK;
                    uint16_t count = usRegStatusBuf[REG_STATUS_CHILD] * REG_INPUT_NREGS;
                    #if MODBUS_DEBUG == 1
                    if (data_archives.debug[THREAD_MODBUS_COMMUNICATE_ID]) {
                        UPRINT("Read %d register\r\n", count);
                    }
                    #endif
                    for (uint16_t i=0; i<count; i=i+REG_INPUT_NREGS) {
                        uint16_t index_start = i;
                        uint16_t number_reg = REG_INPUT_NREGS;
                        if ((index_start) + REG_INPUT_NREGS > count) {
                            number_reg = count - index_start;
                        }
                        uint8_t buffer_result[255];
                        if (mb_send_cmd(buffer_result, usRegStatusBuf[REG_STATUS_ID] + 1, READ_INPUT_REGISTERS, 1000 + index_start, number_reg, NULL, 0) == HAL_OK) {
                            uint16_t pos = 3;
                            for (uint16_t j=0; j<number_reg; j++) {
                                usRegInputBuf[REG_INPUT_NREGS + index_start] = ((uint16_t)buffer_result[pos] << 8) | buffer_result[pos + 1];
                                index_start++;
                                pos = pos + 2;
                            }
                        }
                        else { // timeout
                            result = HAL_ERROR;
                            break;
                        }
                    }

                    if (result == HAL_ERROR) {
                        #if (MODBUS_DEBUG == 1)
                        if (data_archives.debug[THREAD_MODBUS_COMMUNICATE_ID]) {
                            UPRINT("MODBUS_READ_DATA: ERROR TIMEOUT\r\n");
                        }
                        #endif
                        pdu_error_handle();
                        break;
                    }
                    else {
                        if (usRegStatusBuf[REG_STATUS_MODE] == PDU_MODE_MASTER) {
                            // => send to server
                        }
                    }

                    // read status //
                    if (usRegStatusBuf[REG_STATUS_CHILD] == 0) {
                        sys_timer_add_nondata(THREAD_MODBUS_COMMUNICATE_ID, THREAD_MODBUS_READ_DATA, FREQUENCY_READ_DATA - usRegStatusBuf[REG_STATUS_CHILD]*50, false);
                        break;
                    }
                    uint8_t result_status[10];
                    if (mb_send_cmd(result_status, usRegStatusBuf[REG_STATUS_ID] + 1, READ_HOLDING_REGISTERS, REG_STATUS_STATUS + REG_START, 1, NULL, 0) == HAL_OK) {
                        usRegStatusBuf[REG_STATUS_STATUS] = ((uint16_t)result_status[3] << 8) | result_status[4];
                        if (usRegStatusBuf[REG_STATUS_STATUS] == PDU_ERROR) {
                            #if MODBUS_DEBUG == 1
                            if (data_archives.debug[THREAD_MODBUS_COMMUNICATE_ID]) {
                                UPRINT("READ STATUS: PDU_ERROR\r\n");
                            }
                            #endif
                            pdu_error_handle();
                        }
                        else {
                            sys_timer_add_nondata(THREAD_MODBUS_COMMUNICATE_ID, THREAD_MODBUS_READ_DATA, FREQUENCY_READ_DATA - usRegStatusBuf[REG_STATUS_CHILD]*50, false);
                            // thread_push_messenger_nondata(THREAD_MODBUS_COMMUNICATE_ID, THREAD_MODBUS_READ_DATA);
                        }
                    }
                    else {
                        #if MODBUS_DEBUG == 1
                        if (data_archives.debug[THREAD_MODBUS_COMMUNICATE_ID]) {
                            UPRINT("ERR: read status\r\n");
                        }
                        #endif
                        pdu_error_handle();
                    }
                    break;
                }
                case THREAD_MODBUS_ERROR: {
                    #if MODBUS_DEBUG == 1
                    if (data_archives.debug[THREAD_MODBUS_COMMUNICATE_ID]) {
                        UPRINT("ERR: THREAD_MODBUS_ERROR\r\n");
                    }
                    #endif
                    pdu_error_handle();
                    break;
                }
            } // end switch
            free_messenger(msg);
            // sys_thread_suspend(THREAD_MODBUS_COMMUNICATE_ID);
        }
    }
}

void thread_modbus_communicate_slave_func(void const * arg) {
    usRegStatusBuf[REG_STATUS_ID] = 0;
    usRegStatusBuf[REG_STATUS_MODE] = PDU_MODE_MASTER;
    usRegStatusBuf[REG_STATUS_CHILD] = 0;
    usRegStatusBuf[REG_STATUS_STATUS] = PDU_ERROR;

    eMBInit(MB_RTU, 1, 5, 9600, MB_PAR_NONE);//eMBErrorCode eStatus =
    eMBEnable();
    // ucMBAddress = 0;

    while (1)
    {
        osDelay(10);
        eMBPoll();
    }
}

eMBErrorCode
eMBRegInputCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs )
{
    // UPRINT("rir %d\r\n", usNRegs);
    eMBErrorCode    eStatus = MB_ENOERR;
    int             iRegIndex;
    usAddress--;
    if( ( usAddress >= REG_START ) && ( usAddress + usNRegs <= REG_START + REG_INPUT_NREGS * (usRegStatusBuf[REG_STATUS_CHILD] + 1) ) &&  usRegInputBuf != NULL)
    {
        sys_timer_add_nondata(THREAD_MODBUS_COMMUNICATE_ID, THREAD_MODBUS_ERROR, FREQUENCY_READ_DATA + 500, false);
        iRegIndex = ( int )( usAddress - REG_START );
        while( usNRegs > 0 )
        {
            *pucRegBuffer++ =
                ( unsigned char )( usRegInputBuf[iRegIndex] >> 8 );
            *pucRegBuffer++ =
                ( unsigned char )( usRegInputBuf[iRegIndex] & 0xFF );
            iRegIndex++;
            usNRegs--;
        }
    }
    else
    {
        eStatus = MB_ENOREG;
    }

    return eStatus;
}

eMBErrorCode
eMBRegHoldingCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs, eMBRegisterMode eMode) {
    eMBErrorCode    eStatus = MB_ENOERR;
    usAddress--;
    if(usAddress >= REG_START) {
        uint8_t iRegIndex;
        iRegIndex = usAddress - REG_START;
        if (eMode == MB_REG_WRITE) {
            uint8_t pos = 0;
            for (uint8_t i=0; i<usNRegs; i++) {
                usRegStatusBuf[iRegIndex + i] = (((uint16_t)pucRegBuffer[pos] << 8) | pucRegBuffer[pos + 1]);
                switch (iRegIndex + i) {
                    case REG_STATUS_MODE: {
                        if (usRegStatusBuf[REG_STATUS_MODE] == PDU_MODE_SLAVE) {
                            #if MODBUS_DEBUG == 1
                            if (data_archives.debug[THREAD_MODBUS_COMMUNICATE_ID]) {
                                UPRINT("MODE: Slave\r\n");
                            }
                            #endif
                            sys_timer_remove(THREAD_MODBUS_COMMUNICATE_ID, THREAD_MODBUS_ERROR);
                            sys_timer_remove(THREAD_MODBUS_COMMUNICATE_ID, THREAD_MODBUS_READ_DATA);
                        }
                        break;
                    }
                    case REG_STATUS_ID: {
                        if (usRegStatusBuf[REG_STATUS_MODE] == PDU_MODE_SLAVE) {
                            #if MODBUS_DEBUG == 1
                            if (data_archives.debug[THREAD_MODBUS_COMMUNICATE_ID]) {
                                UPRINT("My ID: %d\r\n", usRegStatusBuf[REG_STATUS_ID]);
                            }
                            #endif
                            ucMBAddress = usRegStatusBuf[REG_STATUS_ID];
                        }
                        else {
                            usRegStatusBuf[REG_STATUS_ID] = 1;
                            break;
                        }
                        if (usRegStatusBuf[REG_STATUS_CHILD] != 0) {
                            uint8_t buffer_result[10];
                            uint8_t data[2];
                            data[0] = (usRegStatusBuf[REG_STATUS_ID] + 1) >> 8;
                            data[1] = (usRegStatusBuf[REG_STATUS_ID] + 1) & 0x00FF;
                            if (mb_send_cmd(buffer_result, 1, WRITE_SINGLE_REGISTER, REG_STATUS_ID + REG_START, 0, data, 2) == HAL_OK) {
                                pucRegBuffer[pos] = buffer_result[4];
                                pucRegBuffer[pos + 1] = buffer_result[5];
                                uint16_t max_id = buffer_result[5];
                                max_id = max_id | ((uint16_t)buffer_result[4] << 8);
                                usRegStatusBuf[REG_STATUS_CHILD] = max_id - usRegStatusBuf[REG_STATUS_ID];
                                #if MODBUS_DEBUG == 1
                                if (data_archives.debug[THREAD_MODBUS_COMMUNICATE_ID]) {
                                    UPRINT("Number child: %d\r\n", usRegStatusBuf[REG_STATUS_CHILD]);
                                }
                                #endif
                                sys_timer_add_nondata(THREAD_MODBUS_COMMUNICATE_ID, THREAD_MODBUS_READ_DATA, usRegStatusBuf[REG_STATUS_CHILD] * FREQUENCY_READ_DATA, false);
                            }
                            else {
                                #if MODBUS_DEBUG == 1
                                if (data_archives.debug[THREAD_MODBUS_COMMUNICATE_ID]) {
                                    UPRINT("ERR: Slave Set ID\r\n");
                                }
                                #endif
                                pdu_error_handle();
                            }
                        }
                        sys_timer_add_nondata(THREAD_MODBUS_COMMUNICATE_ID, THREAD_MODBUS_ERROR, ((usRegStatusBuf[REG_STATUS_CHILD] + 1) * FREQUENCY_READ_DATA) + 500, false);
                        usRegStatusBuf[REG_STATUS_STATUS] = PDU_OK;
                        sys_free(usRegInputBuf);
                        usRegInputBuf = NULL;
                        usRegInputBuf = (USHORT *)sys_malloc(REG_INPUT_NREGS * sizeof(USHORT) * (usRegStatusBuf[REG_STATUS_CHILD] + 1));
                        break;
                    }
                } // switch
                pos = pos + 2;
            } //for
        }
        else if (eMode == MB_REG_READ) {
            while (usNRegs > 0) {
                *pucRegBuffer++ = (unsigned char)(usRegStatusBuf[iRegIndex] >> 8);
                *pucRegBuffer++ = (unsigned char)(usRegStatusBuf[iRegIndex] & 0xFF);
                if (iRegIndex == REG_STATUS_STATUS  && usRegStatusBuf[REG_STATUS_STATUS] == PDU_ERROR) {
                    usRegStatusBuf[REG_STATUS_ID] = 1;
                    ucMBAddress = 1;
                    #if MODBUS_DEBUG == 1
                    if (data_archives.debug[THREAD_MODBUS_COMMUNICATE_ID]) {
                        UPRINT("My ID: %d\r\n", usRegStatusBuf[REG_STATUS_ID]);
                    }
                    #endif
                }
                iRegIndex++;
                usNRegs--;
            }
        }
    }
    else {
        eStatus = MB_ENOREG;
    }
    return eStatus;
}


eMBErrorCode
eMBRegCoilsCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNCoils, eMBRegisterMode eMode ) {
    eMBErrorCode    eStatus = MB_ENOERR;
    if(usAddress >= REG_START) {
        
    }
    else {
        eStatus = MB_ENOREG;
    }
    return eStatus;
}

eMBErrorCode
eMBRegDiscreteCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNDiscrete) {
    return MB_ENOREG;
}
