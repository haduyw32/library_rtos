#ifndef __THREAD_MODBUS_COMMUNICATE_H
#define __THREAD_MODBUS_COMMUNICATE_H

#ifdef __cplusplus
 extern "C" {
#endif 

#define THREAD_MODBUS_FINDSLAVE                     0
#define THREAD_MODBUS_SETID                         1
#define THREAD_MODBUS_REQUEST_SLAVE                 2
#define THREAD_MODBUS_READ_DATA                     3
#define THREAD_MODBUS_ERROR                         4

#define REG_INPUT_NREGS                             84

#define PDU_MODE_MASTER                             0
#define PDU_MODE_SLAVE                              1

#define REG_STATUS_ID                               0
#define REG_STATUS_MODE                             1
#define REG_STATUS_CHILD                            2
#define REG_STATUS_STATUS                           3

typedef enum 
{
  PDU_OK       = 0x00U,
  PDU_ERROR    = 0x01U
} PDU_Status;

void thread_modbus_communicate_func(void const * arg);
void thread_modbus_communicate_slave_func(void const * arg);

#ifdef __cplusplus
}
#endif

#endif /* __THREAD_MODBUS_COMMUNICATE_H */
