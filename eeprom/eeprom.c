#include "system.h"
#include "eeprom.h"

#ifdef STM32F407xx
static uint32_t GetSector(uint32_t Address);
#endif

#ifdef STM32L151xB
HAL_StatusTypeDef eeprom_read(uint32_t address, uint8_t* pbuf, uint32_t len) {
	uint32_t eeprom_address = 0;

	eeprom_address = address + EEPROM_BASE_ADDRESS;

	if ((uint8_t*)pbuf == (uint8_t*)0 || len == 0 || (address + len) > EEPROM_MAX_SIZE) {
		return HAL_ERROR;
	}

	HAL_FLASH_Unlock();
	memcpy(pbuf, (const uint8_t*)eeprom_address, len);
	HAL_FLASH_Lock();
	return HAL_OK;
}

HAL_StatusTypeDef eeprom_write(uint32_t address, uint8_t* pbuf, uint32_t len) {
	uint32_t eeprom_address = 0;
	uint32_t index = 0;
	HAL_StatusTypeDef flash_status;

	eeprom_address = address + EEPROM_BASE_ADDRESS;

	if ((uint8_t*)pbuf == (uint8_t*)0 || len == 0 || (address + len) > EEPROM_MAX_SIZE) {
		return HAL_ERROR;
	}

	HAL_FLASH_Unlock();
	while (index < len) {
		flash_status = HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_BYTE, (eeprom_address + index), (uint8_t)(*(pbuf + index)));

		if (flash_status == HAL_OK) {
			index++;
		}
	}

	HAL_FLASH_Lock();
	return HAL_OK;
}
#endif

HAL_StatusTypeDef flash_read(uint32_t address, uint8_t* pbuf, uint32_t len) {
	if ((uint8_t*)pbuf == (uint8_t*)0 || len == 0 || (address + len) > FLASH_END_ADDR || address < FLASH_START_ADDR) {
		return HAL_ERROR;
	}
    if ((address % 4) != 0) {
		return HAL_ERROR;
	}
	HAL_FLASH_Unlock();
    #ifdef STM32L151xB
	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_BSY | FLASH_FLAG_EOP | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_ENDHV | FLASH_FLAG_READY | FLASH_FLAG_SIZERR | FLASH_FLAG_OPTVERR);
	#endif

	HAL_StatusTypeDef status = HAL_OK;
	for (uint16_t i=0; i<len; i++) {
		status = FLASH_WaitForLastOperation(1000);
		if (status != HAL_OK) {
			break;
		}
		pbuf[i] = (*(__IO uint8_t*) (address + i));
	}
	HAL_FLASH_Lock();
	return status;
}

HAL_StatusTypeDef flash_write(uint32_t address, uint8_t* pbuf, uint32_t len) {
	if ((uint8_t*)pbuf == (uint8_t*)0 || len == 0 || (address + len) > FLASH_END_ADDR || address < FLASH_START_ADDR) {
		return HAL_ERROR;
	}
	if ((address % 4) != 0) {
		return HAL_ERROR;
	}

	HAL_StatusTypeDef status = HAL_OK;
	#ifdef STM32L151xB
	uint32_t addr_start_page = (address / FLASH_PAGE_SIZE) * FLASH_PAGE_SIZE;
	status = flash_page_erase(addr_start_page, len);
	#elif STM32F407xx
	status = flash_page_erase(address, len);
	#endif
  if (status != HAL_OK) {
      return status;
  }
	#ifdef STM32L151xB
	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_BSY | FLASH_FLAG_EOP | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_ENDHV | FLASH_FLAG_READY | FLASH_FLAG_SIZERR | FLASH_FLAG_OPTVERR);
	#endif

	uint32_t temp;
	uint16_t i=0;
	HAL_FLASH_Unlock();
	while (i < len) {
		if (i > len) {
			break;
		}
		else {
			temp = 0;
			temp = temp | ((uint32_t)(pbuf[i]));
			if ((i+1) < len) {
				temp = temp | ((uint32_t)(pbuf[i+1]) << 8);
			}
			if ((i+2) < len) {
				temp = temp | ((uint32_t)(pbuf[i+2]) << 16);
			}
			if ((i+3) < len) {
				temp = temp | ((uint32_t)(pbuf[i+3])  << 24);
			}
			status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, address + i, temp);
			if (status != HAL_OK) {
				break;
			}
		}
		i = i + 4;
	}
	HAL_FLASH_Lock();
	return status;
}

HAL_StatusTypeDef flash_page_erase(uint32_t start_addr, uint32_t len) {
    FLASH_EraseInitTypeDef EraseInitStruct;
    uint32_t PAGEError = 0;
	#ifdef STM32L151xB
	uint8_t total_page = 1;
	if (len > FLASH_PAGE_SIZE) {
		total_page = len / FLASH_PAGE_SIZE;
		if ((len % FLASH_PAGE_SIZE) != 0) {
			total_page++;
		}
	}
	if ((start_addr % FLASH_PAGE_SIZE) != 0) {
		printf("ERROR: flash_page_earse -> Incorrect \"start_addr\"\r\n");
		return HAL_ERROR;
	}
    HAL_FLASH_Unlock();
    __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_BSY | FLASH_FLAG_EOP | FLASH_FLAG_ENDHV | FLASH_FLAG_READY | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_SIZERR | FLASH_FLAG_OPTVERR);
    EraseInitStruct.TypeErase = FLASH_TYPEERASE_PAGES;
    EraseInitStruct.PageAddress = start_addr;
    EraseInitStruct.NbPages = number_page;//(end_addr - start_addr) / FLASH_PAGE_SIZE;
    if (HAL_FLASHEx_Erase(&EraseInitStruct, &PAGEError) != HAL_OK)
    {
      return HAL_ERROR;
    }
    HAL_FLASH_Lock();
	return HAL_OK;
	#elif STM32F407xx
  UNUSED(PAGEError);
	/* Get the 1st sector to erase */
	uint32_t FirstSector = GetSector(start_addr);
	/* Get the number of sector to erase from 1st sector*/
	uint32_t NbOfSectors = GetSector(start_addr) - FirstSector + 1;

	/* Fill EraseInit structure*/
	EraseInitStruct.TypeErase = FLASH_TYPEERASE_SECTORS;
	EraseInitStruct.VoltageRange = FLASH_VOLTAGE_RANGE_3;
	EraseInitStruct.Sector = FirstSector;
	EraseInitStruct.NbSectors = NbOfSectors;

  HAL_FLASH_Unlock();
	if (HAL_FLASHEx_Erase(&EraseInitStruct, &PAGEError) != HAL_OK) {
		return HAL_ERROR;
	}
  HAL_FLASH_Lock();
	return HAL_OK;
  #endif
}

#ifdef STM32F407xx
/**
  * @brief  Gets the sector of a given address
  * @param  None
  * @retval The sector of a given address
  */
static uint32_t GetSector(uint32_t Address)
{
	uint32_t sector = 0;
  
  if((Address < ADDR_FLASH_SECTOR_1) && (Address >= ADDR_FLASH_SECTOR_0))
  {
    sector = FLASH_SECTOR_0;  
  }
  else if((Address < ADDR_FLASH_SECTOR_2) && (Address >= ADDR_FLASH_SECTOR_1))
  {
    sector = FLASH_SECTOR_1;  
  }
  else if((Address < ADDR_FLASH_SECTOR_3) && (Address >= ADDR_FLASH_SECTOR_2))
  {
    sector = FLASH_SECTOR_2;  
  }
  else if((Address < ADDR_FLASH_SECTOR_4) && (Address >= ADDR_FLASH_SECTOR_3))
  {
    sector = FLASH_SECTOR_3;  
  }
  else if((Address < ADDR_FLASH_SECTOR_5) && (Address >= ADDR_FLASH_SECTOR_4))
  {
    sector = FLASH_SECTOR_4;  
  }
  else if((Address < ADDR_FLASH_SECTOR_6) && (Address >= ADDR_FLASH_SECTOR_5))
  {
    sector = FLASH_SECTOR_5;  
  }
  else if((Address < ADDR_FLASH_SECTOR_7) && (Address >= ADDR_FLASH_SECTOR_6))
  {
    sector = FLASH_SECTOR_6;  
  }
  else if((Address < ADDR_FLASH_SECTOR_8) && (Address >= ADDR_FLASH_SECTOR_7))
  {
    sector = FLASH_SECTOR_7;  
  }
  else if((Address < ADDR_FLASH_SECTOR_9) && (Address >= ADDR_FLASH_SECTOR_8))
  {
    sector = FLASH_SECTOR_8;  
  }
  else if((Address < ADDR_FLASH_SECTOR_10) && (Address >= ADDR_FLASH_SECTOR_9))
  {
    sector = FLASH_SECTOR_9;  
  }
  else if((Address < ADDR_FLASH_SECTOR_11) && (Address >= ADDR_FLASH_SECTOR_10))
  {
    sector = FLASH_SECTOR_10;  
  }
  else /* (Address < FLASH_END_ADDR) && (Address >= ADDR_FLASH_SECTOR_11) */
  {
    sector = FLASH_SECTOR_11;
  }

  return sector;
}
#endif
