#ifndef __EEPROM_H
#define __EEPROM_H

#ifdef __cplusplus
 extern "C" {
#endif 

#include <stdint.h>

#ifdef STM32L151xB
#define EEPROM_BASE_ADDRESS         (0x08080000)
#define EEPROM_MAX_SIZE             (0x1000)
#define FLASH_START_ADDR            (0x08000000)
#define FLASH_END_ADDR              (0x0801FF00 + 256)
#elif STM32F407xx
#define ADDR_FLASH_SECTOR_0     ((uint32_t)0x08000000) /* Base @ of Sector 0, 16 Kbytes */
#define ADDR_FLASH_SECTOR_1     ((uint32_t)0x08004000) /* Base @ of Sector 1, 16 Kbytes */
#define ADDR_FLASH_SECTOR_2     ((uint32_t)0x08008000) /* Base @ of Sector 2, 16 Kbytes */
#define ADDR_FLASH_SECTOR_3     ((uint32_t)0x0800C000) /* Base @ of Sector 3, 16 Kbytes */
#define ADDR_FLASH_SECTOR_4     ((uint32_t)0x08010000) /* Base @ of Sector 4, 64 Kbytes */
#define ADDR_FLASH_SECTOR_5     ((uint32_t)0x08020000) /* Base @ of Sector 5, 128 Kbytes */
#define ADDR_FLASH_SECTOR_6     ((uint32_t)0x08040000) /* Base @ of Sector 6, 128 Kbytes */
#define ADDR_FLASH_SECTOR_7     ((uint32_t)0x08060000) /* Base @ of Sector 7, 128 Kbytes */
#define ADDR_FLASH_SECTOR_8     ((uint32_t)0x08080000) /* Base @ of Sector 8, 128 Kbytes */
#define ADDR_FLASH_SECTOR_9     ((uint32_t)0x080A0000) /* Base @ of Sector 9, 128 Kbytes */
#define ADDR_FLASH_SECTOR_10    ((uint32_t)0x080C0000) /* Base @ of Sector 10, 128 Kbytes */
#define ADDR_FLASH_SECTOR_11    ((uint32_t)0x080E0000) /* Base @ of Sector 11, 128 Kbytes */

#define FLASH_START_ADDR               (ADDR_FLASH_SECTOR_0)
#define FLASH_END_ADDR                 (ADDR_FLASH_SECTOR_7 + 0x20000)
#endif

//################################################################################################################
HAL_StatusTypeDef eeprom_read(uint32_t address, uint8_t* pbuf, uint32_t len);
HAL_StatusTypeDef eeprom_write(uint32_t address, uint8_t* pbuf, uint32_t len);

HAL_StatusTypeDef flash_read(uint32_t address, uint8_t* pbuf, uint32_t len);
HAL_StatusTypeDef flash_write(uint32_t address, uint8_t* pbuf, uint32_t len);
HAL_StatusTypeDef flash_page_erase(uint32_t start_addr, uint32_t len);
void FLASH_IRQHandler(void);
//################################################################################################################

#ifdef __cplusplus
}
#endif
#endif
