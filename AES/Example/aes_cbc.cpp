#include <AES.h>

void main()
{
    mAES aes;

    byte *key = (unsigned char *)"0123456789010123";

    byte plain[] = "Add NodeAdd NodeAdd AddNod123456";
    printf("\r\n\r\nplain: %s\r\n", plain);
    int plainLength = strlen((char *)plain); // don't count the trailing /0 of the string !
    aes.calc_size_n_pad(plainLength);
    int padedLength = aes.get_size();
    printf("plainLength: %d\r\n", plainLength);
    printf("padedLength: %d\r\n", padedLength);

    unsigned long long int my_iv = 36753562;
    aes.iv_inc();
    byte iv[N_BLOCK];
    byte cipher[padedLength];
    byte check[plainLength + 1];

    aes.set_IV(my_iv);
    aes.get_IV(iv);
    aes.do_aes_encrypt(plain, plainLength, cipher, key, 128, iv);
    printf("Encryption took: %s\r\n", cipher);
    aes.set_IV(my_iv);
    aes.get_IV(iv);
    aes.do_aes_decrypt(cipher, padedLength, check, key, 128, iv);
    check[plainLength] = '\0';
    printf("Decryption took: %s\r\n", check);
}